# Year-round Day Camp

## Pricing

Type | Price /day | Bundle price /day
---|---:|---:
Regular | $70.00 | $60.00
Hockey Option | $90.00 | $80.00

## Summer Pricing

