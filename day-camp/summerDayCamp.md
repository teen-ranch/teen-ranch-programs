# Summer Day Camp

## Ages

* Junior: 5 - 7
* Senior: 8 - 11

## Price

$320.00 + HST

## Capacity
* Junior: 30
* Senior: 30

## Discount

Save 10% on Summer Camp - Register by February 18, 2019 

## Time

* Drop off - 8:30am
* Pick up - 5:00pm

## Includes

1. Morning snack and lunch

## Notes

1. Outdoor activities are weather permitting.
2. Ice skating is unavailable August 12-16, August 19-23 & August 26-30

## What to bring

* Closed toed shoes and long pants for riding
* Skates, helmet and warm clothes for skating
* Weather appropriate clothes and footwear for outdoor activities
* Swimwear and towel
* Sunscreen and insect repellent
* Water bottle

## Website Text

### Junior

Come and jump into adventure with our Junior Day Camp program! Junior Day campers travel with their leaders around camp to try some of the unique activities that Teen Ranch has to offer. Throughout the week they make new friends and grow while engaging in games, crafts, swimming, pony rides, story time, ice skating and more! Join us this summer for a week full of friendship and fun!

### Senior

Enjoy more of what Teen Ranch has to offer with our Senior Day Camp program!  Our Senior Day campers spend the day with their leaders sampling some of our overnight summer camp programs and activities in a day camp setting. Throughout the week senior day campers will try new things and grow in their skills with activities such as soccer, volleyball, archery, bouldering, swimming, pony rides, ice skating and more! This summer come and join us for a week of full of learning new skills and making friends!